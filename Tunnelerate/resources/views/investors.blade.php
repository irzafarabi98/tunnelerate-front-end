@extends('layouts.master')

@section('content')

    <!-- ======= investors Section ======= -->
    <section id="section- investors" class="mb-20">
      <div class="container">

        <div class="row content mb-20">
          <div class="col-md-4">
                <h3 class="bb-3 pb-20">More than 17,500 islands, 700 languanges, 300 ethnics groups to be tapped</h3>
                <p>We personally see what Indonesia could offer to the world and with this enormous potential, we want to deal with it one step at a time. But we could not do it alone, none of us can. At Tunnelerate, we have our own version of success and that is building ana inclusive Indonesian startup landscape, together. Therefore we are looking to partner with crazy ideas that may have significant impact on society from day one.</p>
          </div>
          <div class="col-md-8">
              <img src="{{ asset('img/photos/landscape-min.jpg') }}" style="border-radius:4px;">
          </div>
        </div>
      </div>
    </section> <!-- End investors Section -->

    <!-- ======= form Section ======= -->
    <section id="section-form" class="mb-60">
        <div class="container">
            <div class="row content form-row">

                <div class="col-md-4 form-left">
                    <h2 class="mb-20">Invest with us.</h2>
                    <p>We are excited to announce that we are still opening up for anyone that wants to take the step with us in order to bring inclusivity to Indonesia startup landscape and inviting individuals, insitutions, and organizations to invest with us. As our fund is focused on investing in well-diversified Indonesian companies from the beginning in order to bring immidiate, sustainable, and long-lasting impact to the society.</p>
                </div> <!-- end form left -->

                <div class="col-md-8 form-right"><form action="" method="POST">
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="name" class="form-control" placeholder="Name">
                        </div>
                        <div class="col">
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Job title">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Company name">
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <textarea type="text" class="form-control" placeholder="Company description"></textarea>
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="LinkedIn url">
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Investor type</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Have you invested in venture?</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Target investment amount</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Select what interest you</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>How did you hear about us</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0">
                        <div class="col">
                            <button class="btn btn-template">Submit &ensp;<i class="fas fa-arrow-right"></i></button>
                        </div>
                    </div> <!-- end form row -->

                </form></div> <!-- end form form -->
            </div>
        </div>
    </section>

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  var header_height = $('#header').outerHeight();
  $('.nav-link').removeClass('active');
  $('#link-investors').addClass('active');
  $('body').css("padding-top", header_height);
});
</script>
@endpush