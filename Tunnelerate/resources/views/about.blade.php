@extends('layouts.master')

@section('content')

    <!-- ======= About Us Section ======= -->
    <section id="section-about" class="mb-40">
      <div class="container">

        <div class="row content">
            <div class="col-md-4">
                <h3 class="bb-3 pb-20">Get to know us more</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tristique, sem ac ultricies malesuada, augue velit aliquam diam, vel posuere dolor eros sed neque. Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
            </div>
            <div class="col-md-8">
                <img src="{{ asset('img/photos/about-min.jpg') }}" style="border-radius:4px;">
            </div>
        </div>
      </div>
    </section> <!-- End About Us Section -->

    <!-- ======= Partners Section ======= -->
    <section id="section-partners">
      <div class="container">

        <div class="row content">
            <div class="col-md-4">
                <h3 class="bb-3 pb-20">Partners</h3>
                <p>Maecenas malesuada sapien id sem varius&ensp; <i color="#f27300" class="fas fa-arrow-right"></i></p>
            </div>
            <div class="col-md-8 row mrl-0">
              <div class="col-sm-6 padd-10">
                <div class="card">
                  <div class="card-body">
                    <center><img src="{{ asset('img/portrait/portrait-1.jpg') }}" class="mb-20"></center>
                    <h5><b>Ivan Arie Sustiawan</b></h5>
                    <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
                  </div>
                </div>
              </div> <!-- End card -->
              <div class="col-sm-6 padd-10">
                <div class="card">
                  <div class="card-body">
                    <center><img src="{{ asset('img/portrait/portrait-2.jpg') }}" class="mb-20"></center>
                    <h5><b>Ayunda Afifa</b></h5>
                    <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
                  </div>
                </div>
              </div> <!-- End card -->
              <div class="col-sm-6 padd-10">
                <div class="card">
                  <div class="card-body">
                    <center><img src="{{ asset('img/portrait/portrait-3.jpg') }}" class="mb-20"></center>
                    <h5><b>Bharat Ongso</b></h5>
                    <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
                  </div>
                </div>
              </div> <!-- End card -->
              <div class="col-sm-6 padd-10">
                <div class="card">
                  <div class="card-body">
                    <center><img src="{{ asset('img/portrait/portrait-4.jpg') }}" class="mb-20"></center>
                    <h5><b>Riswanto</b></h5>
                    <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
                  </div>
                </div>
              </div> <!-- End card -->
            </div>
        </div>
      </div>
    </section><!-- End About Us Section -->
    
    <!-- ======= Team Section ======= -->
    <section id="section-team" class="mb-40">
      <div class="container">
        <h3 class="bb-3 pb-20 mb-20">Team united by interest, driven by purpose</h3>
        <p>Join our team&ensp; <a href="#"><i color="#f27300" class="fas fa-arrow-right"></i></a></p>
        <div class="row content">
          
         <div class="col-sm-4 padd-10">
          <div class="card">
            <div class="card-body">
              <center><img src="{{ asset('img/portrait/portrait-1.jpg') }}" class="mb-20"></center>
              <h5><b>Faisal Fahmi</b></h5>
              <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
            </div>
          </div>
        </div> <!-- End card -->
        
        <div class="col-sm-4 padd-10">
          <div class="card">
            <div class="card-body">
              <center><img src="{{ asset('img/portrait/portrait-1.jpg') }}" class="mb-20"></center>
              <h5><b>Jatin Raisinghani</b></h5>
              <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
            </div>
          </div>
        </div> <!-- End card -->
        
        <div class="col-sm-4 padd-10">
          <div class="card">
            <div class="card-body">
              <center><img src="{{ asset('img/portrait/portrait-2.jpg') }}" class="mb-20"></center>
              <h5><b>Christle Hillary</b></h5>
              <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
            </div>
          </div>
        </div> <!-- End card -->
        
        <div class="col-sm-4 padd-10">
          <div class="card">
            <div class="card-body">
              <center><img src="{{ asset('img/portrait/portrait-1.jpg') }}" class="mb-20"></center>
              <h5><b>Afriyanto Syahputra</b></h5>
              <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
            </div>
          </div>
        </div> <!-- End card -->
        
        <div class="col-sm-4 padd-10">
          <div class="card">
            <div class="card-body">
              <center><img src="{{ asset('img/portrait/portrait-1.jpg') }}" class="mb-20"></center>
              <h5><b>Mahar Catur Ferniza</b></h5>
              <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
            </div>
          </div>
        </div> <!-- End card -->
        
        <div class="col-sm-4 padd-10">
          <div class="card">
            <div class="card-body">
              <center><img src="{{ asset('img/portrait/portrait-2.jpg') }}" class="mb-20"></center>
              <h5><b>Audrey</b></h5>
              <p>Mauris libero ipsum, tincidunt at orci eget, congue egestas eros. Nulla nec metus eu mauris condimentum convallis ornare vel enim.</p>
            </div>
          </div>
        </div> <!-- End card -->

        </div>
      </div>
    </section> <!-- End Team Section -->

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  var header_height = $('#header').outerHeight();
  $('.nav-link').removeClass('active');
  $('#link-about').addClass('active');
  $('body').css("padding-top", header_height);
});
</script>
@endpush