@extends('layouts.master')

@section('content')

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-12">
          <h1 class="display-4 mb-5">Accelerating Your <br>Entrepreneurial Journey</h1>
          <a href="#section-about"><p>Lorem ipsum dolor sit amet, consectetur <i color="#f27300" class="fas fa-arrow-right"></i></p></a>
        </div>
      </div>
    </div>
  </section><!-- End Hero -->

  
    <!-- ======= About Us Section ======= -->
    <section id="section-about" class="ptb-80">
      <div class="container">

        <h3 class="mb-30">How we work</h3>

        <div class="row content mb-40">
            <div class="col-md-4 about-item"><div class="item-content">
                <h3 class="mb-30">Helping you grow</h3>
                <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempus tincidunt nunc nec efficitur. Maecenas malesuada sapien id sem varius, nec fringilla nisl accumsan. Phasellus varius dolor nec gravida auctor. Aenean imperdiet arcu pulvinar ligula luctus rutrum. Duis suscipit urna tellus, sed consectetur risus tincidunt at. Donec ultrices accumsan eros, at viverra mi tincidunt mattis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                <p class="mb-20">Maecenas malesuada sapien id sem varius&ensp; <i color="#f27300" class="fas fa-arrow-right"></i></p>
            </div></div>
            <div class="col-md-4 about-item"><div class="item-content">
                <h3 class="mb-30">Industry expertise</h3>
                <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempus tincidunt nunc nec efficitur. Maecenas malesuada sapien id sem varius, nec fringilla nisl accumsan. Phasellus varius dolor nec gravida auctor. Aenean imperdiet arcu pulvinar ligula luctus rutrum. Duis suscipit urna tellus, sed consectetur risus tincidunt at. Donec ultrices accumsan eros, at viverra mi tincidunt mattis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                <p class="mb-20">Maecenas malesuada sapien id sem varius&ensp; <i color="#f27300" class="fas fa-arrow-right"></i></p>
            </div></div>
            <div class="col-md-4 about-item"><div class="item-content">
                <h3 class="mb-30">Here for the long-run</h3>
                <p class="mb-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tempus tincidunt nunc nec efficitur. Maecenas malesuada sapien id sem varius, nec fringilla nisl accumsan. Phasellus varius dolor nec gravida auctor. Aenean imperdiet arcu pulvinar ligula luctus rutrum. Duis suscipit urna tellus, sed consectetur risus tincidunt at. Donec ultrices accumsan eros, at viverra mi tincidunt mattis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
                <p class="mb-20">Maecenas malesuada sapien id sem varius&ensp; <i color="#f27300" class="fas fa-arrow-right"></i></p>
            </div></div>
        </div>

        <div class="row content">
            <div class="col-md-4">
                <h3 class="bb-3 pb-30">Selected companies within our ecosystem</h3>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('img/materials/logo_japang.jpg') }}">
                        <h4 class="mb-20">Jaring Pangan</h4>
                        <p>Maecenas malesuada sapien id sem varius.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="{{ asset('img/materials/logo_eratani.jpg') }}">
                        <h4 class="mb-20">Eratani</h4>
                        <p>Maecenas malesuada sapien id sem varius.</p>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section><!-- End About Us Section -->

    
    <!-- ======= Jumbotron ======= -->
    <section id="section-jumbotron">
      <div id="jumbotron" class="container"><div id="jumbotron-inner">
        <div class="row vertical-center">
          <div class="col-md-4 padd-20">
            <h1 class="mb-40">
              Join our first cohort Calling all courageous founders
            </h1>
            <p><a href="#"><span class="btn btn-light">Apply here <i color="#f27300" class="fas fa-arrow-right"></i></span></a></p>
          </div>
          <div class="col-md-8">
              <img src="{{ asset('img/photos/business-min.jpg') }}">
          </div>
        </div><!-- end row -->
      </div></div> <!-- End jumbotron -->
    </section><!-- End jumbotron -->

    
    <!-- ======= POV Section ======= -->
    <section id="section-pov">
      <div class="container">
        <div class="row ptb-20">
            <div class="col-md-4">
                <h3 class="bb-3 pb-30 mb-20">Hear our Point of View to get Valuable Insights</h3>
                <p>Maecenas malesuada sapien id sem varius&ensp; <i color="#f27300" class="fas fa-arrow-right"></i></p>
            </div>
            <div class="col-md-8">
              <div class="pov-list bb-3 ptb-40">
                <h4>#3 Point of View by Tunnelerate</h4>
              </div>
              <div class="pov-list bb-3 ptb-40">
                <h4>#2 Point of View by Tunnelerate</h4>
              </div>
              <div class="pov-list ptb-40">
                <h4>#1 Point of View by Tunnelerate</h4>
              </div>
            </div>
        </div>
      </div>
    </section>

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  $('.nav-link').removeClass('active');
  $('#link-home').addClass('active');
});
</script>
@endpush