@extends('layouts.master')

@section('content')

    <!-- ======= Program Section ======= -->
    <section id="section-program" class="mb-20">
      <div class="container">

        <div class="row content mb-20">
          <div class="col-md-4">
              <h3 class="bb-3 pb-20">Accelerator program</h3>
          </div>
          <div class="col-md-8">
            <p>Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Nulla ut quam porta, sodales enim non, dignissim neque. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. Donec vel justo fermentum urna venenatis elementum a et orci. Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. </p>
          </div>
        </div>
        <div class="row content justify-content-center">
            <div class="col-md-10">
              <img src="{{ asset('img/photos/about-min.jpg') }}" style="border-radius:4px;">
            </div>
        </div>
      </div>
    </section> <!-- End Program Section -->

    <!-- ======= philosophy section ======= -->
    <section id="section-philosophy">
      <div class="container">
        <div class="row content">
          <h3 class="mb-30">Our philosophy</h3>

          <div class="col-md-4 philosophy-item">
            <div class="philosophy-inner">
            <h4 class="mb-20">Day 1 and beyond</h4>
            <p>Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
          </div>
          </div> <!-- End philosophy item -->

          <div class="col-md-4 philosophy-item">
            <div class="philosophy-inner">
            <h4 class="mb-20">Huge problem, small steps</h4>
            <p>Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
          </div> <!-- End philosophy item -->
          
          <div class="col-md-4 philosophy-item">
            <div class="philosophy-inner">
            <h4 class="mb-20">Painkillers, not vitamin</h4>
            <p>Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
          </div> <!-- End philosophy item -->

          <div class="col-md-4 philosophy-item">
            <div class="philosophy-inner">
            <h4 class="mb-20">Here to help</h4>
            <p>Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
          </div> <!-- End philosophy item -->

          <div class="col-md-4 philosophy-item">
            <div class="philosophy-inner">
            <h4 class="mb-20">Not only capital</h4>
            <p>Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
          </div> <!-- End philosophy item -->

          <div class="col-md-4 philosophy-item">
            <div class="philosophy-inner">
            <h4 class="mb-20">With open arms</h4>
            <p>Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
            </div>
          </div> <!-- End philosophy item -->

        </div> <!-- end row -->
      </div>
    </section>

    
    <!-- ======= Overview Section ======= -->
    <section id="section-overview" class="mb-40">

      <div class="container mb-20">
        <div class="row content">
          <h3 class="bb-3 pb-30 mb-30">Overview and preferences</h3>
          <div class="col-md-6">
            <p>Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. Donec vel justo fermentum urna venenatis elementum a et orci. </p>
            <p>Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. Donec vel justo fermentum urna venenatis elementum a et orci. Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. </p>
            <p> Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus.</p>
          </div>
          <div class="col-md-6">
            <p> Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus.</p>
            <p>Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. Donec vel justo fermentum urna venenatis elementum a et orci. </p>
            <p>Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. Donec vel justo fermentum urna venenatis elementum a et orci. Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus. </p>
          </div>
        </div>
      </div> <!-- End container -->
      
      <div id="jumbotron" class="container"><div id="jumbotron-inner">
        <div class="row vertical-center">
          <div class="col-md-4 padd-20">
            <h1 class="mb-40">
              Join our first cohort Calling all courageous founders
            </h1>
            <p><a href="#"><span class="btn btn-light">Apply here <i color="#f27300" class="fas fa-arrow-right"></i></span></a></p>
          </div>
          <div class="col-md-8">
              <img src="{{ asset('img/photos/business-min.jpg') }}">
          </div>
        </div><!-- end row -->
      </div></div> <!-- End jumbotron -->

    </section>

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  var header_height = $('#header').outerHeight();
  $('.nav-link').removeClass('active');
  $('#link-program').addClass('active');
  $('body').css("padding-top", header_height);
});
</script>
@endpush