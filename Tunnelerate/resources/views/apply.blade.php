@extends('layouts.master')

@section('content')

    <!-- =======  Section ======= -->
    <section id="section" class="mb-20">
      <div class="container">

        <div class="row content mb-20">
          <div class="col-md-4">
                <h3 class="bb-3 pb-20">Why Tunnelerate</h3>
          </div>
          <div class="col-md-8">
            <p>To say we are builders and founders ourselves sounds like an overused statement. But we cannot find other words to describe who we are. All of Tunnelerate Partners are founders and startup builders with various combined range of experiences in tech, product, operational, strategy, finance, and people & culture management aspects of building companies.</p>
            <p>We are inviting you to our exciting program and let us help you scale and grow your company.</p>
          </div>
        </div>
      </div>
    </section> <!-- End  Section -->

    <!-- ======= form Section ======= -->
    <section id="section" class="mb-60">
        <div class="container">
            <div class="row content form-row">

                <div class="col-md-4">
                    <h3 class="bb-3 pb-20 mb-20">Join us</h3>
                </div> <!-- end form left -->

                <div class="col-md-8"><form action="" method="POST">
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="name" class="form-control" placeholder="Name">
                        </div>
                        <div class="col">
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Job title">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Company name">
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <textarea type="text" class="form-control" placeholder="Company description (Under 50 characters)"></textarea>
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Pitch deck (PDF version)">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Product demo video url">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Problem you are trying to solve">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Solution that you provide">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="How the product works">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Whose the product for">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Who are your competitors">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Where are you incorporated">
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>How long has you been working on this</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Have you participated to another program</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Have you received any form of investment</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="What is your current biggest problem">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="What do you need from this program">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Anything that we need to know">
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0">
                        <div class="col">
                            <button class="btn btn-template float-right">Submit &ensp;<i class="fas fa-arrow-right"></i></button>
                        </div>
                    </div> <!-- end form row -->

                </form></div> <!-- end form form -->
            </div>
        </div>
    </section>

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  var header_height = $('#header').outerHeight();
  $('.nav-link').removeClass('active');
  $('body').css("padding-top", header_height);
});
</script>
@endpush