<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <script defer src="{{ asset('/js/all.min.js') }}"></script> <!-- Font awesome -->

	<!-- Outsourcing CSS -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
	<link href="{{ asset('/css/aos.css') }}" rel="stylesheet"> <!-- animate on scroll -->
    <link href="{{ asset('/css/style_template.css') }}" rel="stylesheet"> <!-- template -->

	<!-- stylesheet -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
	<style>
	</style>

</head>
<body>
<!-- animate on scroll -->
<script src="{{ asset('/js/aos.js') }}"></script>
<script>
AOS.init({ once: true,  easing: 'ease-in-out-sine' });
</script>
<!-- Header -->
@include('layouts.partials.header')
<!-- /.header -->
@yield('content') <!-- Contents here! -->
<!-- Footer -->
@include('layouts.partials.footer')
<!-- /.footer -->

</body>
<script type="text/javascript" src="{{ asset('/js/jquery-3.6.0.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/script.js') }}"></script>
@stack('scripts')
<script type="text/javascript">
</script>