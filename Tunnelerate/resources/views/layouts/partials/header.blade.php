
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container-fluid">

      <div class="row justify-content-center ">
        <div class="col-xl-9 d-flex align-items-center justify-content-lg-between">
          <a href="/index" class="logo me-auto me-lg-0"><img src="{{ asset('/img/logo_tunnelerate-trans.png') }}"></a>

          <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
              <li><a id="link-home" class="nav-link" href="/index">Home</a></li>
              <li><a id="link-about" class="nav-link" href="/about">About us</a></li>
              <li><a id="link-program" class="nav-link" href="/program">Our program</a></li>
              <li><a id="link-partner" class="nav-link" href="/partner">Partner with us</a></li>
              <li><a id="link-investors" href="/investors">Investors</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
          </nav><!-- .navbar -->

          <a href="/apply"><span id="apply-btn" class="btn btn-template">Apply here</span></a>
        </div>
      </div>

    </div>
  </header><!-- End Header -->