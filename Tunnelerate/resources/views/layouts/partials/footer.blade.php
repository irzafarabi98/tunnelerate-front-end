
  <!-- ======= Footer ======= -->
  <footer id="section-footer">
    <div class="container ptb-50">
        <div id="footer-upper" class="row justify-content-center">
            <div class="col-md-4">
                <h3 class="mb-30">Stay up to date on all the latest Tunnelerate news</h3>
                <form action="" method="POST">
                    <input type="name" class="form-control mb-20" placeholder="Name">
                    <input type="email" class="form-control mb-20" placeholder="Email">
                    <button type="submit" class="btn btn-light mb-20">Submit&ensp; <i color="#f27300" class="fas fa-arrow-right"></i></button>
                </form>
            </div>
            <div class="col-md-4 row mrl-0">
                <ul class="col">
                    <a href="#"><li>Maecenas malesuada sapien</li></a>
                    <a href="#"><li>Home</li></a>
                    <a href="#"><li>Our progress</li></a>
                    <a href="#"><li>Partner with us</li></a>
                    <a href="#"><li>Investors</li></a>
                    <a href="#"><li>Apply</li></a>
                </ul>
                <ul class="col">
                    <a href="#"><li>Social Media</li></a>
                    <a href="#"><li>Instagram</li></a>
                    <a href="#"><li>LinkedIn</li></a>
                    <a href="#"><li>Spotify</li></a>
                    <a href="#"><li>Youtube</li></a>
                </ul>
            </div>
            <div class="col-md-4">
                <center><img src="{{ asset('/img/logo_tunnelerate-trans.png') }}" class="mb-20"></center>
                <p>Maecenas malesuada sapien id sem varius, nec fringilla nisl accumsan. Phasellus varius dolor nec gravida auctor. Aenean imperdiet arcu pulvinar ligula luctus rutrum. Duis suscipit urna tellus, sed consectetur risus tincidunt at.</p>
            </div>

        </div> <!-- end upper row -->

        <div id="footer-lower" class="row justify-content-center">
            <span class="col">Aenean imperdiet arcu pulvinar ligula luctus rutrum.</span>
            <span class="col align-right">Duis suscipit urna tellus, sed consectetur risus tincidunt at.</span>
        </div> <!-- end lower row -->
    </div>
</footer><!-- End Footer -->