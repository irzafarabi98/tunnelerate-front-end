@extends('layouts.master')

@section('content')

    <!-- ======= Partner Section ======= -->
    <section id="section-partner" class="mb-20">
      <div class="container">

        <div class="row content mb-20">
          <div class="col-md-4">
              <h3 class="bb-3 pb-20">Partner with us</h3>
          </div>
          <div class="col-md-8">
            <p>Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Nulla ut quam porta, sodales enim non, dignissim neque. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi.</p>
            <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. Donec vel justo fermentum urna venenatis elementum a et orci. Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus.</p>
            <p>Sed vel malesuada orci. Curabitur auctor sollicitudin nunc eu porta. Nulla ut quam porta, sodales enim non, dignissim neque. Vivamus orci dui, congue at lectus eu, commodo pharetra erat. Integer non elit ex. Duis lorem libero, auctor bibendum tristique eu, iaculis a nisi.</p>
            <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. Donec vel justo fermentum urna venenatis elementum a et orci. Pellentesque eu augue magna. Praesent at nunc sem. Quisque at quam malesuada nibh tincidunt volutpat. Morbi volutpat, urna placerat porttitor ultricies, ipsum dui varius sem, eu ullamcorper magna arcu at lacus.</p>
          </div>
        </div>
        <div class="row content justify-content-center">
            <div class="col-md-10">
              <img src="{{ asset('img/photos/partner-min.jpg') }}" style="border-radius:4px;">
            </div>
        </div>
      </div>
    </section> <!-- End Partner Section -->


    <!-- ======= collaborate Section ======= -->
    <section id="section-collaborate">
        <div class="container">

            <h3 class="bb-3 pb-20">Ways we can collaborate together</h3>
            <div class="row content">

                <div class="col-md-3 ptb-20 collaborate-item"><a href="#">
                    <img src="{{ asset('img/photos/mentor-min.jpg') }}" class="mb-20">
                    <h5>Mentors and speakers</h5>
                    <p>Connect and advice our founders.</p>
                </a></div> <!-- end collaborate item -->

                <div class="col-md-3 ptb-20 collaborate-item"><a href="#">
                    <img src="{{ asset('img/photos/software-min.jpg') }}" class="mb-20">
                    <h5>Software tools</h5>
                    <p>Providing working tools for our portfolio startups.</p>
                </a></div> <!-- end collaborate item -->
                
                <div class="col-md-3 ptb-20 collaborate-item"><a href="#">
                    <img src="{{ asset('img/photos/audience-min.jpg') }}" class="mb-20">
                    <h5>Media</h5>
                    <p>Promote exciting up and coming startups to reach bigger audience.</p>
                </a></div> <!-- end collaborate item -->
                
                <div class="col-md-3 ptb-20 collaborate-item"><a href="#">
                    <img src="{{ asset('img/photos/business-min.jpg') }}" class="mb-20">
                    <h5>Venture capital</h5>
                    <p>Get early access to high quality startups for deal flow.</p>
                </a></div> <!-- end collaborate item -->
                
                <div class="col-md-3 ptb-20 collaborate-item"><a href="#">
                    <img src="{{ asset('img/photos/graduation-min.jpg') }}" class="mb-20">
                    <h5>Education institution</h5>
                    <p>Get early access to high quality startups for deal flow.</p>
                </a></div> <!-- end collaborate item -->
                
                <div class="col-md-3 ptb-20 collaborate-item"><a href="#">
                    <img src="{{ asset('img/photos/oversea-min.jpg') }}" class="mb-20">
                    <h5>Overseas partners</h5>
                    <p>Get early access to high quality startups for deal flow.</p>
                </a></div> <!-- end collaborate item -->
                
                <div class="col-md-3 ptb-20 collaborate-item"><a href="#">
                    <img src="{{ asset('img/photos/capital-min.jpg') }}" class="mb-20">
                    <h5>Government entities</h5>
                    <p>Get early access to high quality startups for deal flow.</p>
                </a></div> <!-- end collaborate item -->

            </div>
        </div>
    </section>

    <!-- ======= form Section ======= -->
    <section id="section-form" class="mb-60">
        <div class="container">
            <div class="row content form-row">

                <div class="col-md-4 form-left">
                    <h2 class="mb-20">Be part of our form.</h2>
                    <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam sed nibh eros. Morbi placerat mauris at lorem mattis, nec ultrices erat efficitur. </p>
                </div> <!-- end form left -->

                <div class="col-md-8 form-right"><form action="" method="POST">
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="name" class="form-control" placeholder="Name">
                        </div>
                        <div class="col">
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Mobile number">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Job title">
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Company name">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Company website">
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="LinkedIn profile url">
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Select</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Select</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Select</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Select</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-select">
                                <option selected disabled>Select</option>
                            </select>
                        </div>
                    </div> <!-- end form row -->
                    
                    <div class="row mrl-0 mb-20">
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Text">
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" placeholder="Text">
                        </div>
                    </div> <!-- end form row -->

                    <div class="row mrl-0">
                        <div class="col">
                            <button class="btn btn-template">Submit &ensp;<i class="fas fa-arrow-right"></i></button>
                        </div>
                    </div> <!-- end form row -->

                </form></div> <!-- end form form -->
            </div>
        </div>
    </section>

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
  var header_height = $('#header').outerHeight();
  $('.nav-link').removeClass('active');
  $('#link-partner').addClass('active');
  $('body').css("padding-top", header_height);
});
</script>
@endpush